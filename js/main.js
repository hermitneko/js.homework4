let num1 = Number(prompt("Enter the first number:"));
let num2 = Number(prompt("Enter the second number:"));


while (isNaN(num1) || isNaN(num2)) {
    alert("Try again");
    num1 = Number(prompt("Enter the first number:", 2));
    num2 = Number(prompt("Enter the second number:", 2));
}

let operation = prompt("What action needs to be taken?(+ - * /)");
let result;

function miniCalculator (num1, num2, operation) {
    switch (operation) {
        case '+':
            result = num1 + num2;
            break;
        case '-':
            result = num1 - num2;
            break;
        case '*':
            result = num1 * num2;
            break;
        case '/':
            result = num1 / num2;
            break;
        default:
            result = alert("Wrong operation. Try again.")
    }
    return result;
}

console.log(miniCalculator(num1, num2, operation));
